package com.example.datetimepickerexample

import android.app.Activity
import android.content.ContentResolver
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_contact_picker.*

class ContactPicker : AppCompatActivity() {

    val PICK = 2019

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_picker)

        btn_pick.setOnClickListener { view ->
            var intent = Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
            startActivityForResult(intent, PICK)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK && resultCode == Activity.RESULT_OK) {
            var contact: Uri = data!!.data
            var cursor: Cursor = contentResolver.query(contact, null, null, null)
            cursor.moveToFirst()
            var phone = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
            var name = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)

            var phoneS = cursor.getString(phone)
            var nameS = cursor.getString(name)

            Toast.makeText(this,"$phoneS $nameS",Toast.LENGTH_LONG).show()

        }
    }

}



