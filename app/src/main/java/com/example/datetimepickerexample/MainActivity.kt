package com.example.datetimepickerexample

import android.app.*
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.NotificationCompat
import android.text.format.Time
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun setDate(view: View) {
        var c = Calendar.getInstance()
        var day = c.get(Calendar.DAY_OF_MONTH)
        var month = c.get(Calendar.MONTH)
        var year = c.get(Calendar.YEAR)

        //TODO Picker
        val datePicker = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, month, day ->
            lbl_date.text = "$day/${month + 1}/$year"
        }, year, month, day)
        datePicker.show()
    }

    fun setTime(view: View) {
        var c = Calendar.getInstance()
        var hour = c.get(Calendar.HOUR_OF_DAY)
        var minutes = c.get(Calendar.MINUTE)

        var timePicker = TimePickerDialog(
            this,
            TimePickerDialog.OnTimeSetListener { view, hours, minutes -> lbl_time.text = "$hours:$minutes" },
            hour,
            minutes,
            true
        )
        timePicker.show()

    }

    fun toSnackBarExample(view: View) {
        startActivity(Intent(this, SnackBarActivity::class.java))
    }

    fun toMenuInflateExample(view: View) {
        startActivity(Intent(this, MenuActivity::class.java))
    }

    fun save(s: String) {
        Toast.makeText(applicationContext, s, Toast.LENGTH_LONG).show()
    }

    fun showAlert(view: View) {
        val saveAlert = AlertDialog.Builder(this)
        saveAlert.setTitle("Save")
        saveAlert.setMessage("Are you sure you want to save changes?")
        saveAlert.setPositiveButton("Yes") { dialogInterface: DialogInterface, i: Int ->
            this.save("Yes button")
            dialogInterface.dismiss()
        }
        saveAlert.setNegativeButton("No") { dialogInterface: DialogInterface, i: Int ->
            this.save("No")
            dialogInterface.dismiss()
        }
        saveAlert.setNeutralButton("Remind me later") { dialogInterface: DialogInterface, i: Int ->

            this.save("Remind me later")

        }
        saveAlert.show()
    }

    fun seek(view: View) {
        startActivity(Intent(this, SeekActivity::class.java))

    }

    fun toProgress(view: View) {
        startActivity(Intent(this, ProgressActivity::class.java))
    }

    fun toWebview(view: View) {
        startActivity(Intent(this, WebViewActivity::class.java))
    }

    fun launchNotification(view: View) {
        val id = "my_channel_01"

        val name = getString(R.string.abc_action_bar_home_description)

        val description = getString(R.string.abc_action_bar_home_description)
        val importance = NotificationManager.IMPORTANCE_HIGH
        val mChannel = NotificationChannel(id, name, importance)
        mChannel.description = description
        mChannel.enableLights(true)
        mChannel.enableVibration(true)

        mChannel.enableLights(true)
        mChannel.lightColor = Color.GREEN

        val mBuilder = NotificationCompat.Builder(this, id)
            .setSmallIcon(R.drawable.notification_icon_background)
            .setContentTitle("Uw moeder roept")
            .setContentText("Awel e, wor zitte?? kzit ier mee al da eten")

        val intent = Intent(this, ResultActivity::class.java)

        val stackBuilder = TaskStackBuilder.create(this)

        stackBuilder.addNextIntent(intent);
        val resultPendingAct = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
        mBuilder.setContentIntent(resultPendingAct)
        val mNotiManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        mNotiManager.notify(1, mBuilder.build())

    }

    fun goToContactFinder(view: View) {
        startActivity(Intent(this, ContactPicker::class.java))
    }

    fun sendKill(view: View) {
        val uri = Uri.parse("smsto:0470431232")
        val intent = Intent(Intent.ACTION_SENDTO, uri)
        intent.putExtra("sms_body", "7days ...")
        startActivity(intent)
    }
}
