package com.example.datetimepickerexample

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.SeekBar
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_seek.*

class SeekActivity : AppCompatActivity() {


    lateinit var slider:SeekBar
    lateinit var value: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_seek)

        slider = seekBar
        slider.min = -55
        slider.max = 10
       
        value = btn_noti
        slider.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                value.text = "in progress .... $progress"
            }
            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                value.text = "Tracking started ..." + slider.progress
            }
            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                value.text = "Thank you for selecting " + slider.progress
            }
        })
    }
}
