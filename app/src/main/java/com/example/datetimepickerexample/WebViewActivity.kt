package com.example.datetimepickerexample

import android.content.res.Resources
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_web_view.*

class WebViewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        var primaryColor =this.resources.getColor(R.color.colorPrimaryDark,theme)
        webView.setBackgroundColor(primaryColor)
        webView.settings.loadsImagesAutomatically = true
        webView.settings.javaScriptEnabled = true
        webView.loadUrl("https://www.9gag.com")
    }
}
